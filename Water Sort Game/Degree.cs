﻿using System.Drawing;

namespace Water_Sort_Game
{
    public class Degree : Shape
    {
        int Value { get; set; }
        public Degree(float X, float Y, Color ShapeColor, int Value) : base(X, Y, ShapeColor)
        {
            this.Value = Value;
        }
        public override void Draw(Graphics g)
        {
            if(Value > 0)
            {
                Pen greenPen = new Pen(Color.FromArgb(255, 0, 255, 0), 10);
                g.DrawLine(greenPen, X+15, Y-10, X+45,Y-10);
                greenPen.Dispose();
                if (Value > 1)
                {
                    Pen yellowPen = new Pen(Color.FromArgb(255, 255, 255, 0), 10);
                    g.DrawLine(yellowPen, X + 10, Y-25, X + 50, Y-25);
                    yellowPen.Dispose();
                    if (Value > 2)
                    {
                        Pen redPen = new Pen(Color.FromArgb(255, 255, 0, 0), 10);
                        g.DrawLine(redPen, X + 5, Y - 40, X + 55, Y - 40);
                        redPen.Dispose();
                        if (Value > 3)
                        {
                            Star star = new Star(X + 30, Y - 80, Color.Yellow, 10, 20);
                            star.Draw(g);
                        }
                    }
                }
            }
        }
    }
}
