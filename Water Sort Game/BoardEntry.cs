﻿using System;
using System.Windows.Forms;

namespace Water_Sort_Game
{
    public partial class BoardEntry : Form
    {
        public PointData entry { get; set; }
        public int points { get; set; }
        public int gamemode { get; set; }
        public BoardEntry(int points, int gamemode)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            this.points = points;
            this.gamemode = gamemode;
        }
        private void bContinue_Click(object sender, EventArgs e)
        {
            entry = new PointData(points,mtbName.Text, gamemode);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void bSkip_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void BoardEntry_Load(object sender, EventArgs e)
        {
            lMessage1.Text = String.Format("You scored {0} points!", points);
        }
    }
}
