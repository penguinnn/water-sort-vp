﻿
namespace Water_Sort_Game
{
    partial class BoardEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BoardEntry));
            this.mtbName = new System.Windows.Forms.MaskedTextBox();
            this.lMessage1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bContinue = new System.Windows.Forms.Button();
            this.bSkip = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mtbName
            // 
            this.mtbName.BackColor = System.Drawing.Color.Black;
            this.mtbName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.mtbName.CausesValidation = false;
            this.mtbName.Culture = new System.Globalization.CultureInfo("en-001");
            this.mtbName.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.mtbName.Font = new System.Drawing.Font("Showcard Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtbName.ForeColor = System.Drawing.Color.White;
            this.mtbName.Location = new System.Drawing.Point(768, 315);
            this.mtbName.Mask = "?????";
            this.mtbName.Name = "mtbName";
            this.mtbName.Size = new System.Drawing.Size(140, 40);
            this.mtbName.TabIndex = 0;
            // 
            // lMessage1
            // 
            this.lMessage1.AutoSize = true;
            this.lMessage1.CausesValidation = false;
            this.tableLayoutPanel1.SetColumnSpan(this.lMessage1, 3);
            this.lMessage1.Font = new System.Drawing.Font("Showcard Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMessage1.ForeColor = System.Drawing.Color.White;
            this.lMessage1.Location = new System.Drawing.Point(258, 255);
            this.lMessage1.Name = "lMessage1";
            this.lMessage1.Size = new System.Drawing.Size(371, 40);
            this.lMessage1.TabIndex = 1;
            this.lMessage1.Text = "You scored 0 points!";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.CausesValidation = false;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(258, 312);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 40);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter your name:";
            // 
            // bContinue
            // 
            this.bContinue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bContinue.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bContinue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bContinue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bContinue.Font = new System.Drawing.Font("Showcard Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bContinue.ForeColor = System.Drawing.Color.White;
            this.bContinue.Location = new System.Drawing.Point(525, 379);
            this.bContinue.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.bContinue.Name = "bContinue";
            this.bContinue.Size = new System.Drawing.Size(225, 66);
            this.bContinue.TabIndex = 3;
            this.bContinue.Text = "Continue";
            this.bContinue.UseVisualStyleBackColor = false;
            this.bContinue.Click += new System.EventHandler(this.bContinue_Click);
            // 
            // bSkip
            // 
            this.bSkip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bSkip.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bSkip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bSkip.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bSkip.Font = new System.Drawing.Font("Showcard Gothic", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSkip.ForeColor = System.Drawing.Color.White;
            this.bSkip.Location = new System.Drawing.Point(270, 379);
            this.bSkip.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.bSkip.Name = "bSkip";
            this.bSkip.Size = new System.Drawing.Size(225, 66);
            this.bSkip.TabIndex = 4;
            this.bSkip.Text = "Skip";
            this.bSkip.UseVisualStyleBackColor = false;
            this.bSkip.Click += new System.EventHandler(this.bSkip_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.bSkip, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lMessage1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.bContinue, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.mtbName, 3, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.09566F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.128691F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.841733F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.83825F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.09566F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1276, 707);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // BoardEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1300, 731);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BoardEntry";
            this.Text = "Water Sort";
            this.Load += new System.EventHandler(this.BoardEntry_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox mtbName;
        private System.Windows.Forms.Label lMessage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bContinue;
        private System.Windows.Forms.Button bSkip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}