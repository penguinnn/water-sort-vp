﻿using System.Drawing;
using System.Windows.Forms;

namespace Water_Sort_Game
{
    public partial class LevelGrid : Form
    {
        private LevelTile[] Tiles = new LevelTile[4];
        public LevelGrid()
        {
            InitializeComponent();

            float XStart = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2 - 300;
            float YStart = (float)System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height / 3;
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            Tiles[0] = new LevelTile(60, 250, XStart, YStart, 1);
            Tiles[1] = new LevelTile(60, 250, XStart+150, YStart, 2);
            Tiles[2] = new LevelTile(60, 250, XStart+300, YStart, 3);
            Tiles[3] = new LevelTile(60, 250, XStart+450, YStart, 4);
        }
        private void LevelGrid_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            for (int i = 0; i < Tiles.Length; i++)
            {
                Tiles[i].Draw(e.Graphics);
            }
        }
        private void LevelGrid_MouseUp(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < Tiles.Length; i++)
            {
                if (Tiles[i].IsHit(e.X, e.Y))
                {
                    Level f = new Level(Tiles[i].Degree);
                    this.Hide();
                    if (f.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                    {
                        BoardEntry f1 = new BoardEntry(f.game.points, f.Degree);
                        if (f1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            f1.entry.Save();
                        }
                    }
                    this.Show();
                }
            }
        }
        private void bBack_Click(object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
