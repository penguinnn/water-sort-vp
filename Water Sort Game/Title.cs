﻿using System;
using System.Drawing;

namespace Water_Sort_Game
{
    public class Title : Shape
    {
        string Text { get; set; }
        public Title(float X, float Y, Color ShapeColor, string Text) : base(X, Y, ShapeColor)
        {
            this.Text = Text;
        }
        public override void Draw(Graphics g)
        {
            Font drawFont = new Font("Showcard Gothic", 12);
            SolidBrush drawBrush = new SolidBrush(Color.White);
            StringFormat drawFormat = new StringFormat();
            g.DrawString(Text, drawFont, drawBrush, X, Y, drawFormat);
            drawFont.Dispose();
            drawBrush.Dispose();
        }
    }
}
