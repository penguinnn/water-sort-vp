﻿using System.Drawing;

namespace Water_Sort_Game
{
    public abstract class Shape
    {
        public float X { get; set; }
        public float Y { get; set; }
        public Color ShapeColor { get; set; }
        public Shape(float X, float Y, Color ShapeColor)
        {
            this.X = X;
            this.Y = Y;
            this.ShapeColor = ShapeColor;
        }
        public abstract void Draw(Graphics g);
    }
}
