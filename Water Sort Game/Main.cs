﻿using System;
using System.Windows.Forms;

namespace Water_Sort_Game
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }
        private void bExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void bPlay_Click(object sender, EventArgs e)
        {
            LevelGrid f = new LevelGrid();
            this.Hide();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                this.Show();
            }
        }
        private void bLeaderboard_Click(object sender, EventArgs e)
        {
            Leaderboard f = new Leaderboard();
            this.Hide();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                this.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Help f = new Help();
            this.Hide();
            if (f.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                this.Show();
            }
        }
    }
}
