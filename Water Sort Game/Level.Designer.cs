﻿
namespace Water_Sort_Game
{
    partial class Level
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Level));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.lTime = new System.Windows.Forms.Label();
            this.lPoints = new System.Windows.Forms.Label();
            this.bReset = new System.Windows.Forms.Button();
            this.lGameSate = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button2.CausesValidation = false;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(1320, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(207, 53);
            this.button2.TabIndex = 4;
            this.button2.TabStop = false;
            this.button2.Text = "Quit";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lTime
            // 
            this.lTime.AutoSize = true;
            this.lTime.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTime.Location = new System.Drawing.Point(386, 25);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(132, 50);
            this.lTime.TabIndex = 0;
            this.lTime.Text = "00:00";
            // 
            // lPoints
            // 
            this.lPoints.AutoSize = true;
            this.lPoints.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPoints.Location = new System.Drawing.Point(635, 25);
            this.lPoints.Name = "lPoints";
            this.lPoints.Size = new System.Drawing.Size(163, 50);
            this.lPoints.TabIndex = 2;
            this.lPoints.Text = "Points";
            // 
            // bReset
            // 
            this.bReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bReset.CausesValidation = false;
            this.bReset.FlatAppearance.BorderSize = 0;
            this.bReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bReset.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReset.ForeColor = System.Drawing.Color.Transparent;
            this.bReset.Location = new System.Drawing.Point(1034, 22);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(207, 53);
            this.bReset.TabIndex = 0;
            this.bReset.TabStop = false;
            this.bReset.Text = "Restart";
            this.bReset.UseVisualStyleBackColor = false;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // lGameSate
            // 
            this.lGameSate.AutoSize = true;
            this.lGameSate.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lGameSate.Location = new System.Drawing.Point(12, 25);
            this.lGameSate.Name = "lGameSate";
            this.lGameSate.Size = new System.Drawing.Size(274, 50);
            this.lGameSate.TabIndex = 1;
            this.lGameSate.Text = "Playing for";
            // 
            // Level
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1743, 667);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.lPoints);
            this.Controls.Add(this.lGameSate);
            this.Controls.Add(this.lTime);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(733, 493);
            this.Name = "Level";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Water Sort";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Level_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Level_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lTime;
        private System.Windows.Forms.Label lPoints;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Label lGameSate;
    }
}