﻿using System;
using System.Drawing;

namespace Water_Sort_Game
{
    public class Tube : Square
    {
        public int TubeSize { get; set; }
        public Color[] Colors { get; set; }
        public Color[] InitialColors { get; set; }
        public bool Selected { get; set; }
        public Tube(float X, float Y, Color ShapeColor, int Width, int Height, Color[] Colors, int TubeSize) : base (Width, Height, X, Y, ShapeColor)
        {
            if (TubeSize > 1)
            {
                this.TubeSize = TubeSize;
                this.Colors = new Color[TubeSize];
                this.InitialColors = new Color[TubeSize];
                if (TubeSize >= Colors.Length)
                {
                    for(int i = 0; i < Colors.Length; i++)
                    {
                        this.Colors[i] = Colors[i];
                        this.InitialColors[i] = Colors[i];
                    }
                } else
                {
                    throw new ArgumentException();
                }
            } else
            {
                throw new ArgumentException();
            }
        }

        public bool IsHomo()
        {
            if (!Colors[0].Equals(Color.Transparent))
            {
                Color tmp = Colors[0];
                for (int i = 1; i < TubeSize; i++)
                {
                    if(!Colors[i].Equals(tmp))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        public void reset()
        {
            for (int i = 0; i < Colors.Length; i++)
            {
                this.Colors[i] = InitialColors[i];
            }
        }
        /*public bool IsFull()
        {
            return !Colors[TubeSize - 1].Equals(Color.Transparent);
        }*/
        public override void Draw(Graphics g)
        {
            Pen p = null;
            if (Selected)
            {
                p = new Pen(Color.Red, 5);
            } else
            {
                p = new Pen(Color.White, 5);
            }
            g.DrawLine(p, X, Y - 20, X, Y + Height);
            g.DrawLine(p, X - (float)2.5, Y + Height, X + Width + (float)2.5, Y + Height);
            g.DrawLine(p, X + Width, Y - 20, X + Width, Y + Height);
            p.Dispose();
            for (int i = 0; i < TubeSize; i++)
            {
                Brush b = new SolidBrush(Colors[i]);
                g.FillRectangle(b,X+5,Y+Height-40-((Height/TubeSize)*i),Width-10,35);
                b.Dispose();
            }
            Selected = false;
        }
    }
}
