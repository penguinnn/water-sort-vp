﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Water_Sort_Game
{
    public partial class Level : Form
    {
        public TubeMasterScene game = null;
        public int from = -1;
        public int to = -1;
        public int Degree { get; set; }
        public int highScore { get; set; }
        public Level(int Degree)
        {
            InitializeComponent();
            this.Degree = Degree;
            game = new TubeMasterScene(Degree);
            timer1.Start();
            updatePoints();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            List<PointData> board = PointData.Load();
            this.highScore = 0;
            if(board != null)
            {
                foreach (PointData s in board)
                {
                    if (s.gamemode == Degree)
                    {
                        if (this.highScore < s.points)
                        {
                            this.highScore = s.points;
                        }
                    }
                }
            }
        }
        private void Level_Paint(object sender, PaintEventArgs e)
        {   e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            e.Graphics.Clear(Color.Black);
            game.Draw(e.Graphics);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            game.tick();
            lTime.Text = string.Format("{0:00}:{1:00}", game.time / 60, game.time % 60);
            updatePoints();
        }
        
        void updatePoints()
        {
            lPoints.Text = String.Format("{0} points", game.points);
            if (!game.IsSolved())
            {
                lGameSate.Text = "PLAYING FOR";
            }
            if (game.IsSolved() && Degree != 4)
            {
                lGameSate.Text = "SOLVED IN";
            }
            if (highScore < game.points && (game.IsSolved() ^ Degree == 4))
            {
                lGameSate.Text = "HIGHSCORE";
            }
        }
        private void bReset_Click(object sender, EventArgs e)
        {
            game.reset();
            timer1.Start();
            Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void Level_MouseDown(object sender, MouseEventArgs e)
        {
            if (!game.IsSolved())
            {
                int selected = -1;
                for (int i = 0; i < game.TubesAmount; i++)
                {
                    if (game.Tubes[i].IsHit(e.X, e.Y))
                    {
                        selected = i;
                    }
                }
                if (selected != -1)
                {
                    if (from == -1)
                    {
                        from = selected;
                        game.Tubes[from].Selected = true;
                        Invalidate();
                    }
                    else if (to == -1)
                    {
                        to = selected;
                        game.MoveTubes(from, to);
                        if (game.IsSolved() && Degree != 4)
                        {
                            timer1.Stop();
                            updatePoints();
                        }
                        else if (game.IsSolved() && Degree == 4)
                        {
                            this.Enabled = false;
                            game = new TubeMasterScene(game.points, game.time);
                            this.Enabled = true;
                        }
                        game.Tubes[from].Selected = false;
                        Invalidate();
                        from = -1;
                        to = -1;
                    }
                }
            }
        }
    }
}
