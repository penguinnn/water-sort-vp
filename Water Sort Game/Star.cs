﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Water_Sort_Game
{
    public class Star : Shape
    {
        public float innerRadius{ get; set; }
        public float outerRadius { get; set; }
        public Star(float X, float Y, Color ShapeColor, float innerRadius, float outerRadius) : base(X, Y, ShapeColor)
        {
            this.innerRadius = innerRadius;
            this.outerRadius = outerRadius;
        }

        public override void Draw(Graphics g)
        {
            PointF[] points = calculateStarPoints(new PointF(X, Y), outerRadius, innerRadius);
            SolidBrush FillBrush = new SolidBrush(ShapeColor);
            g.FillPolygon(FillBrush, points);
            g.DrawPolygon(new Pen(ShapeColor, 5), points);
        }
        private PointF[] calculateStarPoints(PointF center, float outerradius, float innerradius)
        {
            double Ang36 = Math.PI / 5.0;
            double Ang72 = 2.0 * Ang36;
            float Sin36 = (float)Math.Sin(Ang36);
            float Sin72 = (float)Math.Sin(Ang72);
            float Cos36 = (float)Math.Cos(Ang36);
            float Cos72 = (float)Math.Cos(Ang72);
            PointF[] pnts = { center, center, center, center, center, center, center, center, center, center };
            pnts[0].Y -= outerradius;
            pnts[1].X += innerradius * Sin36; pnts[1].Y -= innerradius * Cos36;
            pnts[2].X += outerradius * Sin72; pnts[2].Y -= outerradius * Cos72;
            pnts[3].X += innerradius * Sin72; pnts[3].Y += innerradius * Cos72;
            pnts[4].X += outerradius * Sin36; pnts[4].Y += outerradius * Cos36;
            pnts[5].Y += innerradius;
            pnts[6].X += pnts[6].X - pnts[4].X; pnts[6].Y = pnts[4].Y;
            pnts[7].X += pnts[7].X - pnts[3].X; pnts[7].Y = pnts[3].Y;
            pnts[8].X += pnts[8].X - pnts[2].X; pnts[8].Y = pnts[2].Y;
            pnts[9].X += pnts[9].X - pnts[1].X; pnts[9].Y = pnts[1].Y;
            return pnts;
        }
    }
}
